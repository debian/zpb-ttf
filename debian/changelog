zpb-ttf (1.0.7-1) unstable; urgency=medium

  * New upstream version.
    + Fix recursive compound contour check.
  * d/control: Bump Standards-Version to 4.7.0, no changes needed.
  * d/copyright: Update to current year myself and author upstream.

 -- Leandro Cunha <leandrocunha016@gmail.com>  Mon, 15 Apr 2024 19:13:23 -0300

zpb-ttf (1.0.6-2) unstable; urgency=medium

  * New maintainer. (Closes: #843820)

 -- Leandro Cunha <leandrocunha016@gmail.com>  Sat, 01 Jul 2023 19:15:59 -0300

zpb-ttf (1.0.6-1) unstable; urgency=medium

  * QA upload.
  * New upstream version.

 -- Leandro Cunha <leandrocunha016@gmail.com>  Tue, 20 Jun 2023 02:52:30 -0300

zpb-ttf (1.0.4-2) unstable; urgency=medium

  * QA upload.
  * debian/control:
    - Bumped Standards-Version to 4.6.2.
    - Fixed section of libs to lisp reported by Lintian.
  * debian/upstream/metadata:
    - Removed unknown field reported by Lintian (see
    https://wiki.debian.org/UpstreamMetadata).
  * debian/copyright:
    - Updated years of my contributions.
  * debian/watch:
    - Changed link to dynamically loaded in JavaScript instead of static text.
    - Fixed error to get new version via command uscan and gbp.

 -- Leandro Cunha <leandrocunha016@gmail.com>  Sat, 18 Feb 2023 16:57:43 -0300

zpb-ttf (1.0.4-1) unstable; urgency=medium

  * QA upload.
  * New upstream release. (Closes: #843869)
  * Added debian/gbp.conf.
  * debian/control:
    - Bumped Standards-Version to 4.5.1.
    - Updated debhelper-compat of 12 to 13.
    - Added Multi-Arch: foreign reported by multiarch hinter.
  * debian/copyright:
    - Added myself.
    - Added field Upstream-Contact.
  * debian/upstream/metadata:
    - Added fields Bug-Database, Repository and Repository-Browse.
    - Changed Bug-Submit to GitHub.
  * debian/install:
    - Changed lambda.png to doc/lambda.png.
  * debian/dirs:
    - Removed empty directory usr/share/common-lisp/systems reported by
      Lintian.
  * Updated files and index in doc-base.
  * Added files from the doc folder in debian/docs.

 -- Leandro Cunha <leandrocunha016@gmail.com>  Mon, 18 Jan 2021 15:51:05 -0300

zpb-ttf (0.7-5) unstable; urgency=medium

  * QA upload.
  * debian/copyright: changed packaging licensing from GPL-2+ to BSD-2-Clause.
    (Closes: #956010)
  * debian/upstream/metadata: created.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Fri, 17 Apr 2020 18:20:51 -0300

zpb-ttf (0.7-4) unstable; urgency=medium

  * QA upload.
  * Migrations:
      - debian/copyright to 1.0 format.
      - DebSrc to 3.0.
      - Dropped CDBS.
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 12.
  * debian/control:
      - Added Homepage field.
      - Added 'Rules-Requires-Root: no' to source stanza.
      - Bumped Standards-Version to 4.5.0.
      - Created VCS fields.
      - Improved long description.
      - Improved short description. (Closes: #493430)
  * debian/doc-base: fixed the Section field to point to Programming/Lisp.
  * debian/docs: removed LICENSE file.
  * debian/salsa-ci.yml: added to provide CI tests for Salsa.
  * debian/watch: created.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 05 Apr 2020 21:42:52 -0300

zpb-ttf (0.7-3) unstable; urgency=medium

  * QA upload.
  * Re-upload as source only to target testing.
  * Set Debian QA Group as maintainer. (See #843820)

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 01 Sep 2019 15:19:02 -0300

zpb-ttf (0.7-2) unstable; urgency=low

  * Corrected debhelper dependency

 -- Pierre THIERRY <nowhere.man@levallois.eu.org>  Sun, 11 Mar 2007 21:52:30 +0100

zpb-ttf (0.7-1) unstable; urgency=low

  * Initial release.

 -- Pierre THIERRY <nowhere.man@levallois.eu.org>  Thu,  8 Mar 2007 01:16:44 +0100
